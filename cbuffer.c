#include "cbuffer.h"
#include <stdlib.h>
#include <string.h>

/*************************************************************************************************/

#define CBUFFER_SIZE (sizeof(struct _cbuffer_t))
#define CBUFFER_MEM_ADDR(cbuffer, idx) ((void *)((uint8_t(*)[cbuffer->size])cbuffer->buffer)[idx])

/*************************************************************************************************/

struct _cbuffer_t
{
	size_t qty;
	size_t size;
	void *buffer;
	size_t ihead;
	size_t itail;
	size_t count;
};

/*************************************************************************************************/

cbuffer_t cbuffer_create(size_t qty, size_t size)
{
	cbuffer_t cbuffer = cbuffer_malloc(CBUFFER_SIZE);
	if(cbuffer == NULL)
		return NULL;

	cbuffer->buffer = cbuffer_malloc(size * qty);
	if(cbuffer->buffer == NULL)
	{
		cbuffer_free(cbuffer);
		return NULL;
	}

	cbuffer->qty = qty;
	cbuffer->size = size;
	cbuffer_flush(cbuffer);
	return cbuffer;
}

void cbuffer_destroy(cbuffer_t cbuffer)
{
	if(cbuffer != NULL)
	{
		if(cbuffer->buffer != NULL)
		{
			cbuffer_free(cbuffer->buffer);
		}
		cbuffer_free(cbuffer);
	}
}

uint32_t cbuffer_push(cbuffer_t cbuffer, void *block)
{
	size_t next = (cbuffer->ihead + 1) % cbuffer->qty;
	if(next == cbuffer->itail)
		return 0;

	void *mem_addr = CBUFFER_MEM_ADDR(cbuffer, cbuffer->ihead);
	memcpy(mem_addr, block, cbuffer->size);
	cbuffer->ihead = next;
	cbuffer->count++;
	return 1;
}

uint32_t cbuffer_pop(cbuffer_t cbuffer, void *block)
{
	if(cbuffer->ihead == cbuffer->itail)
		return 0;

	void *mem_addr = CBUFFER_MEM_ADDR(cbuffer, cbuffer->itail);
	memcpy(block, mem_addr, cbuffer->size);
	cbuffer->itail = (cbuffer->itail + 1) % cbuffer->qty;
	cbuffer->count--;
	return 1;
}

uint32_t cbuffer_peek(cbuffer_t cbuffer, void *block)
{
	if(cbuffer->ihead == cbuffer->itail)
		return 0;

	void *mem_addr = CBUFFER_MEM_ADDR(cbuffer, cbuffer->itail);
	memcpy(block, mem_addr, cbuffer->size);
	return 1;
}

void cbuffer_flush(cbuffer_t cbuffer)
{
	cbuffer->ihead = 0;
	cbuffer->itail = 0;
	cbuffer->count = 0;
}

uint32_t cbuffer_status(cbuffer_t cbuffer)
{
	if(cbuffer->ihead == cbuffer->itail)
		return 2;
	else
	{
		size_t next = (cbuffer->ihead + 1) % cbuffer->qty;
		if(next == cbuffer->itail)
			return 0;
		else
			return 1;
	}
}

/*************************************************************************************************/

__attribute__((weak)) void *cbuffer_malloc(size_t size)
{
	return malloc(size);
}

__attribute__((weak)) void cbuffer_free(void *memory)
{
	free(memory);
}