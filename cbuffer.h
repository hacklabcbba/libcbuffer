#ifndef __CBUFFER_H
#define __CBUFFER_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*************************************************************************************************/

typedef struct _cbuffer_t *cbuffer_t;

/*************************************************************************************************/

cbuffer_t cbuffer_create(size_t qty, size_t size);
void cbuffer_destroy(cbuffer_t cbuffer);

uint32_t cbuffer_push(cbuffer_t cbuffer, void *block);
uint32_t cbuffer_pop(cbuffer_t cbuffer, void *block);
uint32_t cbuffer_peek(cbuffer_t cbuffer, void *block);

void cbuffer_flush(cbuffer_t cbuffer);
uint32_t cbuffer_status(cbuffer_t cbuffer);

/*************************************************************************************************/

void *cbuffer_malloc(size_t size);
void cbuffer_free(void *memory);

/*************************************************************************************************/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CBUFFER_H */